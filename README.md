Remix of the herbclip found here: https://www.prusaprinters.org/prints/6806-small-clip-for-herb-bags-other-packaging/files

This one holds the bag a little bit stronger.

![BagClip](BagClip.png "BagClip")
